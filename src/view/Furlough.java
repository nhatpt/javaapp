/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.connector;
import controller.excutequery;
import controller.insertLoger;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.loadDSNghiPhep;
import model.lyDoNghiPhepCB;
import model.phongBan;

/**
 *
 * @author Long
 */
public final class Furlough extends javax.swing.JPanel {

    public ArrayList<lyDoNghiPhepCB> listReason = new ArrayList<>();
    public ArrayList<String> listStaff = new ArrayList<>();
    public static ArrayList<loadDSNghiPhep> listDSNghiPhep = new ArrayList<>();
    public ArrayList<String> endDate = new ArrayList<String>();
    public ArrayList<String> startDate = new ArrayList<String>();
    public int getMalogin = getmalogin();
    public int getmalogin(){
        return Login.maLogin;
    }
    /**
     * /**
     * Creates new form NghiPhep
     */
    DefaultTableModel model = new DefaultTableModel();

    public Furlough() {
        initComponents();
        model = (DefaultTableModel) tbFurlogh.getModel();
        getDBReason();
        settable();
    }

    //Load  Staff's ID text
    private void LoadListDepartment(int x) {
        String sql = "select phongban.tenPB from nhanvien, phongban where nhanvien.maPB = phongban.maPB and nhanvien.maNV=" + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                txtDepartment.setText(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadFullName(int x) {
        String sql = "select nhanvien.hoTen from nhanvien where maNV = " + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                txtFullName.setText(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadEndDate(int x) {
        String sql = "select qlnghiphep.ktNghiphep from qlnghiphep where maNV = " + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                endDate.add(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadStartDate(int x) {
        String sql = "select qlnghiphep.bdNghiphep from qlnghiphep where maNV = " + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                startDate.add(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Load combobox Reason
    public void getDBReason() {
        LoadListReason();
        for (lyDoNghiPhepCB x : listReason) {
            cbReason.addItem(x.getLyDo());
        }
    }

    private void LoadListReason() {
        String sql = "select * from nghiphep";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listReason.add(new lyDoNghiPhepCB(rs.getInt(1), rs.getString(2)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Load Table danh sach
    private static void getDbNghiPhep() {
        String sql = "SELECT qlnp.maqlnghiphep, nv.maNV, nv.hoTen, pb.tenPB, np.lyDo, qlnp.bdNghiphep, qlnp.ktNghiphep\n"
                + "FROM nhanvien nv, phongban pb, nghiphep np, qlnghiphep qlnp\n"
                + "WHERE nv.maPB = pb.maPB AND nv.maNV = qlnp.maNV AND np.maNghiphep = qlnp.maNghiphep";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                listDSNghiPhep.add(new loadDSNghiPhep(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void settable() {
        listDSNghiPhep.clear();
        getDbNghiPhep();
        model.setRowCount(0);
        for (loadDSNghiPhep x : listDSNghiPhep) {
            model.addRow(new Object[]{x.getMaQLNghiPhep(), x.getMaNV(), x.getTenNV(), x.getPhongBan(), x.getLyDo(), x.getFromDate(), x.getToDate()});
        }
    }

    //Get Data From UI
    //Get Reason ID
    private int getLyDo(String x) {
        for (lyDoNghiPhepCB lydo : listReason) {
            if (lydo.getLyDo() == null ? x == null : lydo.getLyDo().equals(x)) {
                return lydo.getMaNghiPhep();
            }
        }
        return -1;
    }

    //Get Deparment ID
    private static int getMaPB(String x) {
        for (phongBan cv : HomePage.listPB) {
            if (cv.getTenPB() == null ? x == null : cv.getTenPB().equals(x)) {
                return cv.getMaPB();
            }
        }
        return 0;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtToDate = new javax.swing.JTextField();
        txtStaffID = new javax.swing.JTextField();
        txtFromDate = new javax.swing.JTextField();
        cbReason = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaAnother = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbFurlogh = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        txtFullName = new javax.swing.JTextField();
        txtDepartment = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();

        setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        setMaximumSize(new java.awt.Dimension(1280, 720));
        setMinimumSize(new java.awt.Dimension(1280, 600));
        setPreferredSize(new java.awt.Dimension(1280, 600));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setText("FURLOGH MANAGEMENT");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("ID No.");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Full Name");

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("Department");

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setText("Reason");

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel6.setText("Another");

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel8.setText("ToDate");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setText("From Date");

        txtStaffID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStaffIDKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStaffIDKeyTyped(evt);
            }
        });

        cbReason.setMinimumSize(new java.awt.Dimension(56, 19));
        cbReason.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbReasonItemStateChanged(evt);
            }
        });

        areaAnother.setColumns(20);
        areaAnother.setRows(5);
        areaAnother.setEnabled(false);
        jScrollPane1.setViewportView(areaAnother);

        jButton1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jButton1.setText("ADD");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel10.setText("(dd/mm/yyyy)");

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel11.setText("(dd/mm/yyyy)");

        tbFurlogh.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        tbFurlogh.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Staff's ID", "Full Name", "Department", "Reason", "From Date", "To Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbFurlogh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbFurloghMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbFurlogh);
        if (tbFurlogh.getColumnModel().getColumnCount() > 0) {
            tbFurlogh.getColumnModel().getColumn(0).setMinWidth(40);
            tbFurlogh.getColumnModel().getColumn(0).setPreferredWidth(40);
            tbFurlogh.getColumnModel().getColumn(0).setMaxWidth(40);
            tbFurlogh.getColumnModel().getColumn(1).setMinWidth(60);
            tbFurlogh.getColumnModel().getColumn(1).setPreferredWidth(60);
            tbFurlogh.getColumnModel().getColumn(1).setMaxWidth(60);
        }

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Search");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setToolTipText("Nhập tên cần tìm");
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        txtFullName.setEnabled(false);

        txtDepartment.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtStaffID, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFullName, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(19, 19, 19)
                                .addComponent(cbReason, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtToDate)
                            .addComponent(txtFromDate, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel9)
                    .addComponent(txtStaffID, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbReason, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8)
                            .addComponent(txtToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(txtFullName, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel7, txtSearch});

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1122, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private static boolean checkDay(String date) {
        if (!date.matches("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/((19|20)\\d\\d)")) {
            return false;
        }
        //        Pattern groups:
        //- Nhóm 1: (0?[1-9]|[12][0-9]|3[01]): bao gồm các ngày dạng 01-09, 10-19, 20-29; 30-31
        //- Nhóm 2: (0?[1-9]|1[012]): bao gồm các tháng dạng 01-09,10, 11, 12
        //- Nhóm 3: ((19|20)\\d\\d): bao gồm các năm dạng 19xx, 20xx
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String bdNghiPhep = txtFromDate.getText();
        String ktNghiPhep = txtToDate.getText();

        //Insert NhanVien
        String maNV = txtStaffID.getText();
        LoadEndDate(Integer.parseInt(maNV));
        LoadStartDate(Integer.parseInt(maNV));
        String lyDo = cbReason.getSelectedItem().toString();
        int maLyDo = getLyDo(lyDo);

        String pb = txtDepartment.getText();
        int maPB = getMaPB(pb);
        String moTa = areaAnother.getText();

        //Xây dựng thêm các ràng buộc
        if (maNV.length() == 0 && bdNghiPhep.length() == 0 && ktNghiPhep.length() == 0) {
            JOptionPane.showMessageDialog(this, "Chưa nhập đủ thông tin");
        } else {
            if (!checkDay(bdNghiPhep) || !checkDay(ktNghiPhep)) {
                JOptionPane.showMessageDialog(this, "Nhập sai thông tin ngày tháng \nVui lồng nhập đúng định dang dd/mm/yyyy");

            } else if (!subTrackDate(bdNghiPhep, ktNghiPhep)) {
                JOptionPane.showMessageDialog(this, "Thời gian nghỉ phép ít nhất là 1 ngày");
            } else if (!compareDate(bdNghiPhep, ktNghiPhep, endDate, startDate)) {
                JOptionPane.showMessageDialog(this, "Thời gian nghỉ phép đã được đăng kí");
            } else {
                String sqlnp = "INSERT INTO qlnghiphep(maNghiphep, maNV, bdNghiphep, ktNghiphep, trangThai, moTa)\n"
                        + "VALUES(" + maLyDo + "," + maNV + ",'" + bdNghiPhep + "','" + ktNghiPhep + "',0, N'" + moTa + "')";
                if (excutequery.excute(sqlnp)) {
                    JOptionPane.showMessageDialog(this, "Success");
                }
                settable();
                //String details = Login.luser + " has inserted the furlough request of Staff's ID: " + maNV;
                HomePage.logger.info(Login.luser + " has inserted the furlough request of Staff's ID: " + maNV);
                insertLoger.insertLoger(getMalogin, Login.luser + " has inserted the furlough request of Staff_ID: " + maNV);
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static boolean subTrackDate(String date1, String date2) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date d1, d2;
        boolean check = false;
        try {
            d1 = formater.parse(date1);
            d2 = formater.parse(date2);
            if (d1.before(d2)) {
                check = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return check;
    }

    public static boolean compareDate(String startDate, String endDate, ArrayList<String> endDateList,  ArrayList<String> startDateList) {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date d1, d2, d3, d4;
        boolean check = true;
        try {
            d1 = formater.parse(startDate);
            d2 = formater.parse(endDate);
            for (int i = 0; i < endDateList.size(); i++) {
                d3 = formater.parse(endDateList.get(i));
                d4 = formater.parse(startDateList.get(i));
                if (((d3.before(d2) || d3.compareTo(d2)==0) && (d3.after(d1) || d3.compareTo(d1)==0)) 
                        || ((d4.before(d2)  || d4.compareTo(d2)==0) && (d4.after(d1) || d4.compareTo(d1)==0)) 
                        || ((d4.before(d1) || d4.compareTo(d1)==0) && (d3.after(d2) || d3.compareTo(d2)==0)) 
                        || ((d4.after(d1) || d4.compareTo(d1)==0) && (d3.before(d2) || d3.compareTo(d2)==0))) {
                        check = false;
                        break;
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return check;
    }

    private void cbReasonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbReasonItemStateChanged
        if (cbReason.getSelectedItem().toString().equals("Khác")) {
            areaAnother.setEnabled(true);
        } else {
            areaAnother.setEnabled(false);
        }
    }//GEN-LAST:event_cbReasonItemStateChanged

    private void tbFurloghMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbFurloghMouseClicked
        // TODO add your handling code here:
        int r = tbFurlogh.getSelectedRow();
        if (r > -1) {
            showDetail(r);
        }
    }//GEN-LAST:event_tbFurloghMouseClicked

    private int CheckId(int x) {
        int count = 0;

        String sql = "select Count(maNV) from nhanvien where maNV =" + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                count = rs.getInt(1);
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;

    }
    private void txtStaffIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStaffIDKeyReleased
        // TODO add your handling code here:   
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            if (txtStaffID.getText().length() == 0) {
                txtDepartment.setText("");
                txtFullName.setText("");
            }
        }
        try {
            String maNV = txtStaffID.getText();
            if (CheckId(Integer.parseInt(maNV)) == 0) {
                txtDepartment.setText("");
                txtFullName.setText("");
            }
            if (isNumeric(maNV)) {
                LoadFullName(Integer.parseInt(maNV));
                LoadListDepartment(Integer.parseInt(maNV));
            } else {
                JOptionPane.showMessageDialog(this, "Notify", "Input incorrect", JOptionPane.ERROR);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_txtStaffIDKeyReleased

    private void txtStaffIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStaffIDKeyTyped
        // TODO add your handling code here:

    }//GEN-LAST:event_txtStaffIDKeyTyped

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            if (txtSearch.getText().length() == 0) {
                settable();
            } else {
                settableFind(txtSearch.getText());
            }
        } else {
            settableFind(txtSearch.getText());
        }
    }//GEN-LAST:event_txtSearchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaAnother;
    private javax.swing.JComboBox<String> cbReason;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbFurlogh;
    private javax.swing.JTextField txtDepartment;
    private javax.swing.JTextField txtFromDate;
    private javax.swing.JTextField txtFullName;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtStaffID;
    private javax.swing.JTextField txtToDate;
    // End of variables declaration//GEN-END:variables

    private void showDetail(int r) {
        txtStaffID.setText(model.getValueAt(r, 1).toString());
        txtFullName.setText(model.getValueAt(r, 2).toString());
        txtDepartment.setText(model.getValueAt(r, 3).toString());
        cbReason.setSelectedItem(model.getValueAt(r, 4).toString());
        txtFromDate.setText(model.getValueAt(r, 5).toString());
        txtToDate.setText(model.getValueAt(r, 6).toString());
    }

    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
	public void settableFind(String a) {
        listDSNghiPhep.clear();
        getDSTimKiem(a);
        model.setRowCount(0);
        for (loadDSNghiPhep x : listDSNghiPhep) {
            model.addRow(new Object[]{x.getMaQLNghiPhep(), x.getMaNV(), x.getTenNV(), x.getPhongBan(), x.getLyDo(), x.getFromDate(), x.getToDate()});
        }
    }
	private static void getDSTimKiem(String x) {
        String sql = "SELECT qlnp.maqlnghiphep, nv.maNV, nv.hoTen, pb.tenPB, np.lyDo, qlnp.bdNghiphep, qlnp.ktNghiphep\n"
                + "FROM nhanvien nv, phongban pb, nghiphep np, qlnghiphep qlnp\n"
                + "WHERE nv.maPB = pb.maPB AND nv.maNV = qlnp.maNV AND np.maNghiphep = qlnp.maNghiphep and qlnp.maNV = " + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listDSNghiPhep.add(new loadDSNghiPhep(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
