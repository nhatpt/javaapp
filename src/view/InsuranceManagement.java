/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.connector;
import controller.excutequery;
import controller.insertLoger;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.baoHiem;
import model.loadBaoHiem;

/**
 *
 * @author ThLongLy
 */
public class InsuranceManagement extends javax.swing.JPanel {

    /**
     * Creates new form InsuranceManagement
     */
    DefaultTableModel model = new DefaultTableModel();
    public static ArrayList <loadBaoHiem> listBHNV = new ArrayList<>();
    int maBH = 0;
    
    public InsuranceManagement() {
        initComponents();
        model = (DefaultTableModel) jTable1.getModel();
        getDBBH();
        settable();
    }
    
    public void getDBBH() {
        for (baoHiem x : HomePage.listBHNV) {
           cb_issues.addItem(x.getLoaiBH()); 
        }    
    }
    
    private static void getDbBaoHiem() { 
        String sql = "SELECT bh.maBH, nv.maNV, nv.hoTen, bh.loaiBH, bh.bdBH, bh.ktBH\n"
                + "FROM nhanvien nv, baohiem bh\n"
                + "WHERE nv.maNV = bh.maNV";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                listBHNV.add(new loadBaoHiem(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),rs.getString(6)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void settable() {
        listBHNV.clear();
        getDbBaoHiem();
        model.setRowCount(0);
        for (loadBaoHiem x : listBHNV) {
            model.addRow(new Object[]{x.getMaBH(), x.getMaNV(), x.getHoTen(), x.getLoaiBH(), x.getBdBH(), x.getKtBH()});
        }
    }
    
    private static boolean checkDay(String date) {
        if (!date.matches("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/((19|20)\\d\\d)")) {
            return false;
        }
        //        Pattern groups:
        //- Nhóm 1: (0?[1-9]|[12][0-9]|3[01]): bao gồm các ngày dạng 01-09, 10-19, 20-29; 30-31
        //- Nhóm 2: (0?[1-9]|1[012]): bao gồm các tháng dạng 01-09,10, 11, 12
        //- Nhóm 3: ((19|20)\\d\\d): bao gồm các năm dạng 19xx, 20xx
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
    
    public void showDetail(int r){
        txtStaffId.setText(model.getValueAt(r, 1).toString());
        txtStaffName.setText(model.getValueAt(r, 2).toString());
        cb_issues.setSelectedItem(model.getValueAt(r, 3).toString());
        txtStartDate.setText(model.getValueAt(r, 4).toString());
        txtEndDate.setText(model.getValueAt(r, 5).toString());
    }
    
    private int CheckId(int x) {
        int count = 0;

        String sql = "select Count(maNV) from nhanvien where maNV =" + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                count = rs.getInt(1);
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;

    }
    
    private void LoadFullName(int x) {
        String sql = "select nhanvien.hoTen from nhanvien where maNV = " + x;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                txtStaffName.setText(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
    
    public ArrayList <String> listLoaiBH = new ArrayList<>();
    
    private void getInsuranceType(int maNV)
    {
        String sqlLoaiBH = "select loaiBH from baohiem where maNV = " + maNV;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sqlLoaiBH);

            while (rs.next()) {
                listLoaiBH.add(rs.getString(1));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private boolean checkInsuranceType(String loaiBH){
        boolean check = false;
        if(listLoaiBH.contains(loaiBH)){
            check = true;
        }
        return check;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtEndDate = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtStaffId = new javax.swing.JTextField();
        btn_add = new javax.swing.JButton();
        txtStaffName = new javax.swing.JTextField();
        btn_delete = new javax.swing.JButton();
        cb_issues = new javax.swing.JComboBox<>();
        txtStartDate = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtSearchBar = new javax.swing.JTextField();
        btn_search = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(1280, 600));
        setMinimumSize(new java.awt.Dimension(1280, 600));
        setPreferredSize(new java.awt.Dimension(1280, 600));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Insurance Management");
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        txtEndDate.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Staff's Name");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Staff's ID");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Insurance Type");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Start Date");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("End Date");

        txtStaffId.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtStaffId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStaffIdKeyReleased(evt);
            }
        });

        btn_add.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btn_add.setText("Add Insurance Info");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add(evt);
            }
        });

        txtStaffName.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtStaffName.setEnabled(false);

        btn_delete.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btn_delete.setText("Delete Insurance Info");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_delete(evt);
            }
        });

        cb_issues.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        cb_issues.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bảo hiểm rủi ro tai nạn", "Bảo hiểm y tế", "An toàn tài chính", "Kế hoạch hưu trí", "Tiết kiệm và đầu tư", "Bảo hiểm trọn gói" }));

        txtStartDate.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        jLabel8.setText("(dd/MM/yyyy)");

        jLabel9.setText("(dd/MM/yyyy)");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(71, 71, 71)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtStaffId, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                    .addComponent(txtStaffName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(91, 91, 91)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cb_issues, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_add)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_delete))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtEndDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                            .addComponent(txtStartDate, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))))
                .addGap(24, 24, 24))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_issues, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtStaffId, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtStaffName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_add)
                    .addComponent(btn_delete))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtEndDate, txtStartDate});

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Insurance's ID", "Staff's ID", "Staff's Name", "Insurance Type", "Start Date", "End Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1062, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 198, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Search");

        txtSearchBar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearchBar.setToolTipText("Nhập tên cần tìm");

        btn_search.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/img/search.png"))); // NOI18N
        btn_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addGap(26, 26, 26)
                .addComponent(txtSearchBar, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_search, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtSearchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_search, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_search, jLabel7, txtSearchBar});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(443, 443, 443)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_delete(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delete
        // TODO add your handling code here:
        if(maBH == 0){
            JOptionPane.showMessageDialog(this, "Please select staff for deleting Insurance", "Warning Notification", JOptionPane.WARNING_MESSAGE);
        }else{
            String sqlbh = "delete baohiem where maBH = " + maBH;
            int select = JOptionPane.showConfirmDialog(this, "Are you sure for deleting this Insurance?", "Confirm Notification", JOptionPane.YES_NO_OPTION);
            if (select == JOptionPane.YES_OPTION) {
                excutequery.excute(sqlbh);
            }
            settable();
            HomePage.logger.info(Login.luser +" has deleted the insurance of Staff: " + txtStaffId.getText());
            insertLoger.insertLoger(Login.maLogin, Login.luser +" has deleted the insurance of Staff: " + txtStaffId.getText());

        }
    }//GEN-LAST:event_btn_delete

    private void btn_add(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add
        int maNV = Integer.parseInt(txtStaffId.getText());
        String loaiBH = cb_issues.getSelectedItem().toString();
        String bdBH = txtStartDate.getText();
        String ktBH = txtEndDate.getText();
        getInsuranceType(maNV);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            if(checkDay(bdBH) && checkDay(ktBH))
            {
                if(formatter.parse(bdBH).before(formatter.parse(ktBH)))
                {
                    if(!checkInsuranceType(loaiBH))
                    {
                        String sqlbh = "INSERT INTO baohiem(maNV, loaiBH, bdBH, ktBH)\n" +
                            "VALUES(?, ? , ?, ?)";
                    try {
                        //kết nối csdl:
                        connector.openconnection();
                        PreparedStatement pr = connector.con.prepareStatement(sqlbh);
                        pr.setInt(1, maNV);
                        pr.setString(2, loaiBH);
                        pr.setString(3, bdBH);
                        pr.setString(4, ktBH);
                        
                        pr.executeUpdate();
                        
                        connector.con.close();
                        
                        settable();
                        
                    } catch (Exception ex) {
                       ex.printStackTrace();
                    }
                    
                    JOptionPane.showMessageDialog(this, "Success.");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(this, "Please select the diferent Insurance", "Warning Notification", JOptionPane.WARNING_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "The start date is later than the end date, please re-enter it.");
                }
            }
            else{
                JOptionPane.showMessageDialog(this, "Start date or End date is not formatted correctly.");
            }
                
        } catch (ParseException ex) {
            Logger.getLogger(InsuranceManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        HomePage.logger.info(Login.luser +" has inserted the insurance of Staff: " + txtStaffId.getText());
        insertLoger.insertLoger(Login.maLogin, Login.luser +" has inserted the insurance of Staff: " + txtStaffId.getText());


    }//GEN-LAST:event_btn_add
        
    private void btn_search(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_search

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int row = jTable1.getSelectedRow();
        if(row > -1){
            showDetail(row);
        }
        maBH = Integer.parseInt(model.getValueAt(row, 0).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtStaffIdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStaffIdKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE){
            if(txtStaffId.getText().length() == 0){
                txtStaffName.setText("");
            }
        }
        try {
            String maNV = txtStaffId.getText();
            if (CheckId(Integer.parseInt(maNV)) == 0 ) {
                txtStaffName.setText("");
            }
            if (isNumeric(maNV)) {
                LoadFullName(Integer.parseInt(maNV));
            } else {
                JOptionPane.showMessageDialog(this, "Notify", "Input incorrect", JOptionPane.ERROR);
            }
        } catch (Exception e) {
        
        }
    }//GEN-LAST:event_txtStaffIdKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_search;
    private javax.swing.JComboBox<String> cb_issues;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtEndDate;
    private javax.swing.JTextField txtSearchBar;
    private javax.swing.JTextField txtStaffId;
    private javax.swing.JTextField txtStaffName;
    private javax.swing.JTextField txtStartDate;
    // End of variables declaration//GEN-END:variables
}
