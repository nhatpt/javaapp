/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import org.apache.log4j.Logger;
import controller.connector;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.baoHiem;
import model.chucVu;
import model.luongCB;
import model.phongBan;
import model.trinhDoHocVan;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Hades
 */
public class HomePage extends javax.swing.JFrame {

    /**
     * Creates new form HomePage
     */
    final static Logger logger = Logger.getLogger(HomePage.class);
    public static ArrayList<chucVu> listCV = getDBCVu();
    public static ArrayList<phongBan> listPB = getDBPB();
    public static ArrayList<luongCB> listLuong = getDBLuong();
    public static ArrayList<trinhDoHocVan> listTDHV = getDBTDHV();
    public static ArrayList<baoHiem> listBHNV = getDBBH();

    public int malogin = getmaLogin();
 
    public int getmaLogin(){
       return Login.maLogin;
    }
    
    private static ArrayList<baoHiem> getDBBH() {
        ArrayList<baoHiem> li = new ArrayList<>();
        String sql = "select * from baohiem";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                li.add(new baoHiem(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    private static ArrayList<chucVu> getDBCVu() {
        ArrayList<chucVu> li = new ArrayList<>();
        String sql = "select * from chucvu";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                li.add(new chucVu(rs.getInt(1), rs.getString(2)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    private static ArrayList<phongBan> getDBPB() {
        ArrayList<phongBan> li = new ArrayList<>();
        String sql = "select * from phongban";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                li.add(new phongBan(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    private static ArrayList<luongCB> getDBLuong() {
        ArrayList<luongCB> li = new ArrayList<>();
        String sql = "select * from luong";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                li.add(new luongCB(rs.getInt(1), rs.getDouble(2)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    private static ArrayList<trinhDoHocVan> getDBTDHV() {
        ArrayList<trinhDoHocVan> li = new ArrayList<>();
        String sql = "select * from trinhdohocvan";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                li.add(new trinhDoHocVan(rs.getInt(1), rs.getString(2)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;
    }

    public void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("img/logocty.png")));
    }

    public HomePage() {
        initComponents();
        BasicConfigurator.configure();
        PropertyConfigurator.configure(getClass().getResource("/view/log4j.properties"));

        setLocationRelativeTo(null);
        setIcon();
        setResizable(false);
        pack();
        HomeImage hoimg = new HomeImage();
        body.setBottomComponent(hoimg);

        pack();
    }

    public HomePage(boolean checkRole, boolean logout, boolean login) {
        initComponents();
        BasicConfigurator.configure();
        PropertyConfigurator.configure(getClass().getResource("/view/log4j.properties"));
        setLocationRelativeTo(null);
        setIcon();
        mnCEO.setEnabled(checkRole);

        jmenuLogin.setEnabled(login);
        jmenuLogout.setEnabled(logout);
        
        setResizable(false);
        pack();
        HomeImage hoimg = new HomeImage();
        body.setBottomComponent(hoimg);
        pack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();
        body = new javax.swing.JSplitPane();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jmenuLogin = new javax.swing.JMenuItem();
        jmenuLogout = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        mnCEO = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuDiary = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();

        jCheckBoxMenuItem2.setSelected(true);
        jCheckBoxMenuItem2.setText("jCheckBoxMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("HR Management");
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        getContentPane().setLayout(new java.awt.CardLayout());

        body.setBackground(new java.awt.Color(255, 255, 255));
        body.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 48)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/img/logocty.png"))); // NOI18N
        jLabel2.setText("HUMAN RESOURCE MANAGEMENT");
        jLabel2.setMaximumSize(new java.awt.Dimension(1280, 70));
        jLabel2.setMinimumSize(new java.awt.Dimension(1280, 70));
        jLabel2.setPreferredSize(new java.awt.Dimension(1280, 70));
        jLabel2.setRequestFocusEnabled(false);
        jLabel2.setVerifyInputWhenFocusTarget(false);
        body.setTopComponent(jLabel2);

        jScrollPane1.setMaximumSize(new java.awt.Dimension(1280, 600));
        jScrollPane1.setMinimumSize(new java.awt.Dimension(1280, 600));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(1280, 600));
        body.setBottomComponent(jScrollPane1);

        getContentPane().add(body, "card2");

        jMenu1.setText("System");

        jMenuItem11.setText("Save");
        jMenu1.add(jMenuItem11);

        jmenuLogin.setText("Log in");
        jmenuLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenuLoginActionPerformed(evt);
            }
        });
        jMenu1.add(jmenuLogin);

        jmenuLogout.setText("Log out");
        jmenuLogout.setEnabled(false);
        jmenuLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenuLogoutActionPerformed(evt);
            }
        });
        jMenu1.add(jmenuLogout);

        jMenuItem7.setText("Exit");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Manage");

        jMenuItem1.setText("Staff");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenu5.setText("Salary");

        jMenuItem2.setText("Salary Staff");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem2);

        jMenuItem3.setText("Update CTTL");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem3);

        jMenu2.add(jMenu5);

        jMenu6.setText("Insurance");

        jMenuItem4.setText("Insurance Management");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem4);

        jMenuItem5.setText("Renew Insurance");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem5);

        jMenu2.add(jMenu6);

        jMenuItem6.setText("Furlough");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuBar1.add(jMenu2);

        mnCEO.setText("CEO manager");
        mnCEO.setEnabled(false);

        jMenuItem8.setText("Furlough");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        mnCEO.add(jMenuItem8);

        jMenuItem9.setText("Insurance");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        mnCEO.add(jMenuItem9);

        jMenuItem13.setText("Signup account");
        jMenuItem13.setToolTipText("Create user for HR staff");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        mnCEO.add(jMenuItem13);

        jMenuDiary.setText("Diary of HR staff");
        jMenuDiary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuDiaryActionPerformed(evt);
            }
        });
        mnCEO.add(jMenuDiary);

        jMenuBar1.add(mnCEO);

        jMenu4.setText("Help");
        jMenuBar1.add(jMenu4);

        jMenu7.setText("About");
        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            createStaff st = new createStaff();
            body.setBottomComponent(st);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            salary slr = new salary();
            body.setBottomComponent(slr);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            Furlough frl = new Furlough();
            body.setBottomComponent(frl);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            InsuranceManagement ism = new InsuranceManagement();
            body.setBottomComponent(ism);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            RenewInsuranceManagement rism = new RenewInsuranceManagement();
            body.setBottomComponent(rism);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            insertSalary fuslr = new insertSalary();
            body.setBottomComponent(fuslr);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            FurloughManagement fm = new FurloughManagement();
            body.setBottomComponent(fm);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:

        if (!Login.luser.isEmpty()) {
            c_InsurranceManagement cim = new c_InsurranceManagement();
            body.setBottomComponent(cim);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jmenuLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenuLogoutActionPerformed
        // TODO add your handling code here:
        Login.luser = "";
        HomePage hp = new HomePage(false, false, true);
        hp.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jmenuLogoutActionPerformed

    private void jmenuLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenuLoginActionPerformed
        // TODO add your handling code here:
        Login lg = new Login();
        lg.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jmenuLoginActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            SignupUser su = new SignupUser();
            body.setBottomComponent(su);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuDiaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuDiaryActionPerformed
        // TODO add your handling code here:
        if (!Login.luser.isEmpty()) {
            Diary diary = new Diary();
            body.setBottomComponent(diary);
            pack();
        } else {
            JOptionPane.showMessageDialog(this, "Cần phải đăng nhập");
        }
    }//GEN-LAST:event_jMenuDiaryActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomePage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSplitPane body;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuDiary;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem jmenuLogin;
    private javax.swing.JMenuItem jmenuLogout;
    private javax.swing.JMenu mnCEO;
    // End of variables declaration//GEN-END:variables
}
