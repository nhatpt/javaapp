/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.connector;
import controller.excutequery;
import controller.insertLoger;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.chucVu;
import model.loadNhanVien;
import model.luongCB;
import model.phongBan;
import model.trinhDoHocVan;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Phuc Cao
 */
public class createStaff extends javax.swing.JPanel {

    /**
     * Creates new form createStaff
     */
    DefaultTableModel model = new DefaultTableModel();
    public static ArrayList<loadNhanVien> listNV = new ArrayList<>();
    public static ArrayList<loadNhanVien> listNVSearched = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    char testCellphoneNo = '0';
    connector kn = new connector();

    private void clearAll() {
        txtCMND.setText("");
        txtDiaChi.setText("");
        txtID.setText("");
        txtName.setText("");
        txtPhone.setText("");
        cbChucvu.setSelectedItem(null);
        cbHV.setSelectedItem(null);
        cbPB.setSelectedItem(null);
        cbQueQuan.setSelectedItem(null);
        dobPicker.getEditor().setText("");
        csdPicker.getEditor().setText("");
        cedPicker.getEditor().setText("");
        buttonGroup1.clearSelection();
    }

    private static void getSearchedNhanVien(String sample) {
        String sql = "SELECT *\n"
                + "FROM nhanvien "
                + "WHERE hoTen like N'%"
                + sample + "%'";
        System.out.println(sql);
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                listNVSearched.add(new loadNhanVien(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void getDbNhanvien() {
        String sql = "SELECT nv.maNV, nv.hoTen, nv.GT, nv.SDT, cv.tenCV, nv.diaChi, pb.tenPB, nv.CMND\n"
                + "FROM nhanvien nv, phongban pb, chucvu cv\n"
                + "WHERE nv.maPB = pb.maPB AND nv.maCV = cv.maCV";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                listNV.add(new loadNhanVien(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public createStaff() {
        initComponents();
        model = (DefaultTableModel) jTable1.getModel();
        getDBPB();
        getDBCV();
        getDBTDHV();
        txtID.setEnabled(false);
        settable();
        dobPicker.setFormats(new String[]{"dd/MM/yyyy"});
        csdPicker.setFormats(new String[]{"dd/MM/yyyy"});
        cedPicker.setFormats(new String[]{"dd/MM/yyyy"});
        cbChucvu.setSelectedItem(null);
        cbHV.setSelectedItem(null);
        cbPB.setSelectedItem(null);
        dobPicker.getEditor().setEditable(false);
        csdPicker.getEditor().setEditable(false);
        cedPicker.getEditor().setEditable(false);
    }

    public void getDBTDHV() {
        for (trinhDoHocVan x : HomePage.listTDHV) {
            cbHV.addItem(x.getTenTDHV());
        }
    }

    public void getDBPB() {
        for (phongBan x : HomePage.listPB) {
            cbPB.addItem(x.getTenPB());
        }
    }

    public void getDBCV() {
        for (chucVu x : HomePage.listCV) {
            cbChucvu.addItem(x.getTenCV());
        }
    }

    public void settable() {
        listNV.clear();
        getDbNhanvien();
        model.setRowCount(0);
        for (loadNhanVien x : listNV) {
            model.addRow(new Object[]{x.getMaNV(), x.getHoTen(), x.getGT(), x.getSDT(), x.getTenCV(), x.getDiaChi(), x.getTenPB()});
        }
    }

    public void setSearchTable(String sample) {
        listNVSearched.clear();
        getSearchedNhanVien(sample);
        model.setRowCount(0);
        for (loadNhanVien x : listNVSearched) {
            model.addRow(new Object[]{x.getMaNV(), x.getHoTen(), x.getGT(), x.getSDT(), x.getTenCV(), x.getDiaChi(), x.getTenPB()});
        }
    }

    public boolean checkCMND(String CMND) {
        for (loadNhanVien x : listNV) {
            if (x.getCMND().equals(CMND)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkSDT(String SDT) {
        for (loadNhanVien x : listNV) {
            if (x.getSDT().equals(SDT)) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtDiaChi = new javax.swing.JTextField();
        txtID = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtCMND = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        rbtFemale = new javax.swing.JRadioButton();
        rbMale = new javax.swing.JRadioButton();
        cbPB = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cbChucvu = new javax.swing.JComboBox<String>();
        cbHV = new javax.swing.JComboBox<String>();
        dobPicker = new org.jdesktop.swingx.JXDatePicker();
        csdPicker = new org.jdesktop.swingx.JXDatePicker();
        cedPicker = new org.jdesktop.swingx.JXDatePicker();
        cbQueQuan = new javax.swing.JComboBox();
        Clear = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(1280, 600));
        setMinimumSize(new java.awt.Dimension(1280, 600));
        setPreferredSize(new java.awt.Dimension(1280, 600));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("STAFF PROFILE");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Profile"));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Gender");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Staff ID");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText("Address");

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setText("Phone No.");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setText("Date of Birth");

        txtDiaChi.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        txtID.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Position");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setText("Department");

        txtPhone.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("ID No.");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Home Town");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Education Status");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setText("Full Name");

        txtName.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        txtCMND.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtFemale);
        rbtFemale.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        rbtFemale.setText("Female");

        buttonGroup1.add(rbMale);
        rbMale.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        rbMale.setText("Male");

        cbPB.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setText("Contract Start Date");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Contract End Date");

        cbQueQuan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "An Giang", "Bà Rịa - Vũng Tàu", "Bình Dương", "Bình Phước", "Bình Thuận", "Bình Định", "Bạc Liêu", "Bắc Giang", "Bắc Kạn", "Bắc Ninh", "Bến Tre", "Cao Bằng", "Cà Mau", "Cần Thơ", "Điện Biên", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Nội", "Hà Tĩnh", "Hòa Bình", "Hưng Yên", "Hải Dương", "Hải Phòng", "Hậu Giang", "Hồ Chí Minh", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Long An", "Lào Cai", "Lâm Đồng", "Lạng Sơn", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Thanh Hóa", "Thái Bình", "Thái Nguyên", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Tây Ninh", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái" }));
        cbQueQuan.setSelectedIndex(-1);

        Clear.setText("Clear Fields");
        Clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtID, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                        .addComponent(txtName)
                        .addComponent(txtCMND, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(rbtFemale)
                            .addGap(18, 18, 18)
                            .addComponent(rbMale)))
                    .addComponent(txtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                        .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel4)
                    .addComponent(jLabel15))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbChucvu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dobPicker, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(csdPicker, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cedPicker, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbPB, 0, 200, Short.MAX_VALUE)
                            .addComponent(cbHV, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbQueQuan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(Clear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnUpdate))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cbQueQuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(cbHV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(txtCMND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(rbtFemale)
                            .addComponent(rbMale)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbChucvu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(dobPicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18)
                            .addComponent(cbPB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(csdPicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnUpdate)
                        .addComponent(btnAdd)
                        .addComponent(Clear))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)
                        .addComponent(cedPicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbChucvu, txtPhone});

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Profile List"));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Staff ID", "Full Name", "Gender", "Phone No.", "Position", "Address", "Department"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(10);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1180, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtSearch.setToolTipText("Nhập tên cần tìm");
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/view/img/search.png"))); // NOI18N
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Search");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(498, 498, 498))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(854, 854, 854)
                        .addComponent(jLabel5)
                        .addGap(30, 30, 30)
                        .addComponent(txtSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 32, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnSearch, jLabel5, txtSearch});

    }// </editor-fold>//GEN-END:initComponents

    private static int getMaCvu(String x) {
        for (chucVu cv : HomePage.listCV) {
            if (cv.getTenCV() == null ? x == null : cv.getTenCV().equals(x)) {
                return cv.getMaCV();
            }
        }
        return 0;
    }

    private static int getMaTDHV(String x) {
        for (trinhDoHocVan cv : HomePage.listTDHV) {
            if (cv.getTenTDHV() == null ? x == null : cv.getTenTDHV().equals(x)) {
                return cv.getMaTDHv();
            }
        }
        return 0;
    }

    private static int getMaPB(String x) {
        for (phongBan cv : HomePage.listPB) {
            if (cv.getTenPB() == null ? x == null : cv.getTenPB().equals(x)) {
                return cv.getMaPB();
            }
        }
        return 0;
    }

    private static int selectIDNV(String sql) {
        int id = 0;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy vấn trả về result
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        if (csdPicker.getDate().toString().isEmpty() || cedPicker.getDate().toString().isEmpty() || txtName.getText().isEmpty() || txtCMND.getText().isEmpty()
                || txtPhone.getText().isEmpty() || dobPicker.getDate().toString().isEmpty() || txtDiaChi.getText().isEmpty() || cbQueQuan.getSelectedItem().toString().isEmpty()
                || (!rbMale.isSelected() && !rbtFemale.isSelected())) {
            JOptionPane.showMessageDialog(this, "Please fill all fields!");
        } else if (!checkCMND(txtCMND.getText())) {
            JOptionPane.showMessageDialog(this, "CMND existed!");
        } else if (csdPicker.getDate().after(cedPicker.getDate())) {
            JOptionPane.showMessageDialog(this, "Invalid Contract end date!");
        } else if (!((txtPhone.getText().length() == 10 || txtPhone.getText().length() == 11) && txtPhone.getText().charAt(0) == testCellphoneNo)) {
            JOptionPane.showMessageDialog(this, "Invalid phone number!");
        } else if (!checkSDT(txtPhone.getText())) {
            JOptionPane.showMessageDialog(this, "Phone number existed!");
        } else {
            //Insert Hợp đồng:
            String bdHopDong = sdf.format(csdPicker.getDate());
            String ktHopDong = sdf.format(cedPicker.getDate());

            //Insert NhanVien
            String name = txtName.getText();
            double cmnd = Double.parseDouble(txtCMND.getText());
            String phone = txtPhone.getText();
            String ngaySinh = sdf.format(dobPicker.getDate());
            String diaChi = txtDiaChi.getText();
            String queQuan = cbQueQuan.getSelectedItem().toString();

            String cv = cbChucvu.getSelectedItem().toString();
            int maCV = getMaCvu(cv);

            String tdhv = cbHV.getSelectedItem().toString();
            int maTDHV = getMaTDHV(tdhv);

            String pb = cbPB.getSelectedItem().toString();
            int maPB = getMaPB(pb);

            String GT = null;
            if (rbMale.isSelected()) {
                GT = "Nam";
            } else if (rbtFemale.isSelected()) {
                GT = "Nữ";
            }
            //Xây dựng thêm các ràng buộc

            //Thực hiện tuần tự, nếu không sẽ không được update
            String sqlnv = "INSERT INTO nhanvien(hoTen, CMND, GT, SDT, ngaySinh, diaChi,queQuan, maCV, maTDHV, maPB)\n"
                    + "VALUES(N'" + name + "'," + cmnd + ",N'" + GT + "', '" + phone + "','" + ngaySinh + "', N'" + diaChi + "',N'" + queQuan + "'," + maCV + "," + maTDHV + "," + maPB + ")";
            excutequery.excute(sqlnv);

            JOptionPane.showMessageDialog(this, "Success.");
            settable();

            String sqlid = "select MAX(manv) from nhanvien";
            int maNV = selectIDNV(sqlid);

            String sqlHopDong = "INSERT INTO hopdong(maNV,bdHopdong, ktHopdong) VALUES (" + maNV + ",'" + bdHopDong + "', '" + ktHopDong + "')";
            excutequery.excute(sqlHopDong);
            HomePage.logger.info(Login.luser +" has inserted a staff.");
            insertLoger.insertLoger(Login.maLogin, Login.luser +" has inserted a staff.");

        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (txtSearch.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Vui long nhap ten nhan vien can tim!");
        } else {
            setSearchTable(txtSearch.getText());
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        try {
            // TODO add your handling code here:
            String bdHopDong = csdPicker.getEditor().getText();
            String ktHopDong = cedPicker.getEditor().getText();
            Date checkbdHD = sdf.parse(bdHopDong);
            Date checkktHD = sdf.parse(ktHopDong);
            System.out.println(cbChucvu.getEditor().getItem().toString());
            if (csdPicker.getDate().toString().isEmpty() || cedPicker.getDate().toString().isEmpty() || txtName.getText().isEmpty() || txtCMND.getText().isEmpty()
                    || txtPhone.getText().isEmpty() || dobPicker.getDate().toString().isEmpty() || txtDiaChi.getText().isEmpty() || cbQueQuan.getSelectedItem().toString().isEmpty()
                    || (!rbMale.isSelected() && !rbtFemale.isSelected())) {
                JOptionPane.showMessageDialog(this, "Please fill all fields!");
            } else if (checkbdHD.after(checkktHD)) {
                JOptionPane.showMessageDialog(this, "Invalid Contract end date!");

            } else if (!((txtPhone.getText().length() == 10 || txtPhone.getText().length() == 11) && txtPhone.getText().charAt(0) == testCellphoneNo)) {
                JOptionPane.showMessageDialog(this, "Invalid phone number!");
            } else {
                System.out.println("something 1");
                //Update Hợp đồng:

                System.out.println("something 2");
                //Update NhanVien
                int row = jTable1.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(null, "Select a staff!");
                }
                int value = (int) jTable1.getModel().getValueAt(row, 0);
                String name = txtName.getText();
                int cmnd = Integer.parseInt(txtCMND.getText());
                String phone = txtPhone.getText();
                String ngaySinh = dobPicker.getEditor().getText();
                String diaChi = txtDiaChi.getText();
                String queQuan = cbQueQuan.getSelectedItem().toString();
                System.out.println("something 3");

                String cv = cbChucvu.getSelectedItem().toString();
                System.out.println(cv);
                int maCV = getMaCvu(cv);

                String tdhv = cbHV.getSelectedItem().toString();
                int maTDHV = getMaTDHV(tdhv);

                String pb = cbPB.getSelectedItem().toString();
                int maPB = getMaPB(pb);

                String GT = null;
                if (rbMale.isSelected()) {
                    GT = "Nam";
                } else if (rbtFemale.isSelected()) {
                    GT = "Nữ";
                }
                //Xây dựng thêm các ràng buộc

                //Thực hiện tuần tự, nếu không sẽ không được update
                String sqlnv = "UPDATE nhanvien set "
                        + "hoTen = N'" + name + "',"
                        + "CMND = '" + cmnd + "',"
                        + "GT = N'" + GT + "',"
                        + "diaChi = N'" + diaChi + "',"
                        + "SDT = '" + phone + "',"
                        + "ngaySinh = '" + ngaySinh + "',"
                        + "queQuan = N'" + queQuan + "',"
                        + "maCV = '" + maCV + "',"
                        + "maTDHV = '" + maTDHV + "',"
                        + "maPB = '" + maPB + "'\n"
                        + "WHERE maNV = " + value;
                System.out.println(sqlnv);
                excutequery.excute(sqlnv);

                JOptionPane.showMessageDialog(this, "Success.");
                settable();

                String sqlHopDong = "UPDATE hopdong set "
                        + "bdHopDong = '" + bdHopDong + "',"
                        + "ktHopDong = '" + ktHopDong + "'"
                        + "WHERE maNV = " + value;
                System.out.println(sqlHopDong);
                excutequery.excute(sqlHopDong);
            }
        } catch (ParseException ex) {
            Logger.getLogger(createStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HomePage.logger.info(Login.luser +" has updated the information of Staff: " + txtID);
        insertLoger.insertLoger(Login.maLogin, Login.luser +" has updated the information of Staff: " + txtID);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:


    }//GEN-LAST:event_jTable1MouseClicked

    private void ClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearActionPerformed
        // TODO add your handling code here:
        clearAll();
    }//GEN-LAST:event_ClearActionPerformed

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
        // TODO add your handling code here:
        int row = jTable1.getSelectedRow();
        int value = (int) jTable1.getModel().getValueAt(row, 0);
        String sql = "SELECT nv.maNv, nv.hoTen, nv.CMND, nv.GT,  nv.diaChi, nv.SDT, cv.tenCV, nv.ngaySinh, hd.bdHopdong, hd.ktHopdong, nv.queQuan, hv.tenTDHV, pb.tenPB\n"
                + "FROM nhanvien nv, phongban pb, chucvu cv, hopdong hd, trinhdohocvan hv\n"
                + "WHERE nv.maPB = pb.maPB AND nv.maCV = cv.maCV and hd.maNV = nv.maNV and hv.maTDHV = nv.maTDHV and nv.maNV = '" + value + "'";

        try {
            connector.openconnection();
            Statement st = connector.con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                txtID.setText(String.valueOf(rs.getInt(1)));
                txtName.setText(rs.getString(2));
                txtCMND.setText(rs.getString(3));
                if (rs.getString(4).equals("Nữ")) {
                    rbtFemale.setSelected(true);
                } else {
                    rbMale.setSelected(true);
                }
                txtDiaChi.setText(rs.getString(5));
                txtPhone.setText(rs.getString(6));
                cbChucvu.setSelectedItem(rs.getString(7));

                dobPicker.setDate(sdf.parse(rs.getString(8)));
                csdPicker.setDate(sdf.parse(rs.getString(9)));
                cedPicker.setDate(sdf.parse(rs.getString(10)));

                cbQueQuan.setSelectedItem(rs.getString(11));
                cbHV.setSelectedItem(rs.getString(12));
                cbPB.setSelectedItem(rs.getString(13));
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_jTable1MousePressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            if (txtSearch.getText().length() == 0) {
                settable();
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtSearch.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Vui long nhap ten nhan vien can tim!");
            } else {
                setSearchTable(txtSearch.getText());
            }
        }

    }//GEN-LAST:event_txtSearchKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Clear;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbChucvu;
    private javax.swing.JComboBox<String> cbHV;
    private javax.swing.JComboBox cbPB;
    private javax.swing.JComboBox cbQueQuan;
    private org.jdesktop.swingx.JXDatePicker cedPicker;
    private org.jdesktop.swingx.JXDatePicker csdPicker;
    private org.jdesktop.swingx.JXDatePicker dobPicker;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JRadioButton rbMale;
    private javax.swing.JRadioButton rbtFemale;
    private javax.swing.JTextField txtCMND;
    private javax.swing.JTextField txtDiaChi;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
