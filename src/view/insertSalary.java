/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.connector;
import controller.insertLoger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Loadsalary;
import model.luongCB;
import static view.salary.listSalary;

/**
 *
 * @author NgocChau
 */
public class insertSalary extends javax.swing.JPanel {

    /**
     * Creates new form formulaSalary
     */
    public insertSalary() {
        initComponents();
        model = (DefaultTableModel) jTable1.getModel();
        getDBLuong();
        settable();
    }
    DefaultTableModel model = new DefaultTableModel();
    public static ArrayList<Loadsalary> listSalary2 = new ArrayList<>();
    int maNV = 0;

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private static void getDBSalary() {
        String sql = "SELECT nv.maNV, nv.hoTen, nv.CMND, cv.tenCV,pb.tenPB\n"
                + "FROM nhanvien nv, phongban pb, chucvu cv\n"
                + "WHERE nv.maPB = pb.maPB AND nv.maCV = cv.maCV AND nv.maQLLuong IS NULL";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                listSalary2.add(new Loadsalary(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDBLuong() {
        for (luongCB x : HomePage.listLuong) {
            cbLuongCoBan.addItem(Double.toString(x.getLuongCoBan()));
        }
    }

    public void settable() {
        listSalary2.clear();
        getDBSalary();
        model.setRowCount(0);
        for (Loadsalary x : listSalary2) {
            model.addRow(new Object[]{x.getMaNV(), x.getHoTen(), x.getCMND(), x.getTenCV(), x.getTenPB()});
        }
    }

    private int checkBacLuong(double luongcb) {
        int bacluong = 0;
        String sql = "select bacluong from luong where luongcoban = " + luongcb;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
            bacluong = rs.getInt(1);
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bacluong;
    }

    private int checkEndSalaryID() {
        int check = 0;
        String sql = "SELECT TOP 1 maQLLuong FROM qlluong ORDER BY maQLLuong DESC";
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy cấn trả về resault
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
            check = rs.getInt(1);
            }
            connector.con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        btnOk = new javax.swing.JButton();
        lbMSSV7 = new javax.swing.JLabel();
        lbMSSV9 = new javax.swing.JLabel();
        txthesoluong = new javax.swing.JTextField();
        txtphucap = new javax.swing.JTextField();
        cbLuongCoBan = new javax.swing.JComboBox<>();

        setMaximumSize(new java.awt.Dimension(1280, 600));
        setPreferredSize(new java.awt.Dimension(1280, 600));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("SALARY FORMULA");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Staff ID", "Full Name", "ID No.", "Position", "Departman"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Insert Salary", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Basic Salary");

        btnOk.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnOk.setText("OK");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        lbMSSV7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbMSSV7.setText("Salary Factor");

        lbMSSV9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbMSSV9.setText("Allowance");

        txthesoluong.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        txtphucap.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbMSSV7)
                                    .addComponent(lbMSSV9))
                                .addGap(18, 18, 18))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(27, 27, 27)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cbLuongCoBan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtphucap, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txthesoluong, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txthesoluong, txtphucap});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbLuongCoBan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMSSV7)
                    .addComponent(txthesoluong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMSSV9)
                    .addComponent(txtphucap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(btnOk)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txthesoluong, txtphucap});

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 838, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(470, 470, 470)
                .addComponent(jLabel1)
                .addGap(470, 470, 470))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        // TODO add your handling code here:
        String luongcb = cbLuongCoBan.getSelectedItem().toString(); //1.3 với 1.39
        String heSoLuong = txthesoluong.getText();
        String heSoPhuCap = txtphucap.getText();
        int maQLLuong = checkEndSalaryID();

        double bacluong = checkBacLuong(Double.parseDouble(luongcb));
        if (jTable1.getSelectedRow() > -1) {
            if (heSoLuong.length() != 0 && heSoPhuCap.length() != 0) {
                if (isNumeric(heSoLuong) && isNumeric(heSoPhuCap)) {
                    listSalary.clear();
                    String query = "INSERT INTO qlluong(bacLuong,heSoluong, heSophucap) VALUES (?,?,?);";

                    String sql = "Update nhanvien\n"
                            + "Set maQLLuong = ?\n"
                            + "Where maNV = ?";
                    try {
                        connector.openconnection();

                        PreparedStatement pr = connector.con.prepareStatement(query);
                        pr.setDouble(1, bacluong);
                        pr.setDouble(2, Double.parseDouble(heSoLuong));
                        pr.setDouble(3, Double.parseDouble(heSoPhuCap));
                        //Excute
                        pr.executeUpdate();

                        PreparedStatement pr2 = connector.con.prepareStatement(sql);
                        pr2.setInt(1, maQLLuong+1);        
                        pr2.setInt(2, lMaNV);
                        
                        pr2.executeUpdate();

                        connector.con.close();
                        JOptionPane.showMessageDialog(this, "Cập nhập thành công");
                        settable();
                        HomePage.logger.info(Login.luser +" has insert salary for Staff_ID: " + lMaNV);
                        insertLoger.insertLoger(Login.maLogin, Login.luser +" has insert salary for Staff_ID: " + lMaNV);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Vui lòng nhập số");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Vui lòng không bỏ trống thông tin");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Cần chọn một cột.");
        }
    }//GEN-LAST:event_btnOkActionPerformed

    int lMaNV = 0;
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int r = jTable1.getSelectedRow();
        if (r > -1) {
            lMaNV = Integer.parseInt(model.getValueAt(r, 0).toString());
        }
    }//GEN-LAST:event_jTable1MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOk;
    private javax.swing.JComboBox<String> cbLuongCoBan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lbMSSV7;
    private javax.swing.JLabel lbMSSV9;
    private javax.swing.JTextField txthesoluong;
    private javax.swing.JTextField txtphucap;
    // End of variables declaration//GEN-END:variables
}
