/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Statement;


/**
 *
 * @author Hades
 */
public class excutequery {
    public static boolean excute (String sql){
        boolean check = false;
        try {
            //kết nối csdl:
            connector.openconnection();
            Statement st = connector.con.createStatement();
            //Chạy câu lệnh truy vấn trả về result
            if (st.execute(sql)) {
                connector.con.close();
                check = true;
            }
            else
                check = false;
        }       
        catch (Exception e) {
            e.printStackTrace();
        }    
        return check;
    }
}
