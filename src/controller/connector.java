/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hades
 */
public class connector {
    // Đối tượng connection để tạo kết nối
    public static Connection con;

    public static void openconnection() throws ClassNotFoundException {
        //Chuỗi kết nối với CƠ Sở dữ liệu
        String URL = "jdbc:sqlserver://localhost:1433;databaseName=QLNS";
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String username = "sa";
        String password = "123";
        try {
            //Mở kết nối
            Class.forName(driver);
            //Kiểm tra thông tin kết nối
            con = DriverManager.getConnection(URL, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(connector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
