/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class LoginUser {
    int malogin;
    String username;
    String pwd;
    int role;

    public int getMalogin() {
        return malogin;
    }

    public LoginUser(int malogin, String username, String pwd, int role) {
        this.malogin = malogin;
        this.username = username;
        this.pwd = pwd;
        this.role = role;
    }

    public LoginUser() {
    }

    public String getPwd() {
        return pwd;
    }

    public int getRole() {
        return role;
    }

  

    public String getUsername() {
        return username;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setUsername(String username) {
        this.username = username;
    }
     
}
