/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class loadNhanVien {
    private int maNV;
    private String hoTen;
    private String GT;
    private String SDT;
    private String tenCV;
    private String diaChi;
    private String tenPB;
    private String CMND;

    public loadNhanVien() {
    }

    public loadNhanVien(int maNV, String hoTen, String GT, String SDT, String tenCV, String diaChi, String tenPB, String CMND) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.GT = GT;
        this.SDT = SDT;
        this.tenCV = tenCV;
        this.diaChi = diaChi;
        this.tenPB = tenPB;
        this.CMND = CMND;
    }
    
    public loadNhanVien(int maNV, String hoTen, String GT, String SDT, String tenCV) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.GT = GT;
        this.SDT = SDT;
        this.tenCV = tenCV;
    }

    
    public loadNhanVien(int maNV, String hoTen, String GT, String SDT, String tenCV, String diaChi, String tenPB) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.GT = GT;
        this.SDT = SDT;
        this.tenCV = tenCV;
        this.diaChi = diaChi;
        this.tenPB = tenPB;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    

    public String getDiaChi() {
        return diaChi;
    }

    public String getGT() {
        return GT;
    }

    public String getHoTen() {
        return hoTen;
    }

    public int getMaNV() {
        return maNV;
    }

    public String getSDT() {
        return SDT;
    }

    public String getTenCV() {
        return tenCV;
    }

    public String getTenPB() {
        return tenPB;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public void setGT(String GT) {
        this.GT = GT;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public void setTenCV(String tenCV) {
        this.tenCV = tenCV;
    }

    public void setTenPB(String tenPB) {
        this.tenPB = tenPB;
    }
    
}
