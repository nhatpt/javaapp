/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Long
 */
public class lyDoNghiPhepCB {
    private int maNghiPhep;
    private String lyDo;

    public lyDoNghiPhepCB() {
    }

    public lyDoNghiPhepCB(int maNghiPhep, String lyDo) {
        this.maNghiPhep = maNghiPhep;
        this.lyDo = lyDo;
    }

    public int getMaNghiPhep() {
        return maNghiPhep;
    }

    public String getLyDo() {
        return lyDo;
    }

    public void setLyDo(String lyDo) {
        this.lyDo = lyDo;
    }

    public void setMaNghiPhep(int maNghiPhep) {
        this.maNghiPhep = maNghiPhep;
    }
    
}
