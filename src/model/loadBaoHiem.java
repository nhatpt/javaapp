/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ThLongLy
 */
public class loadBaoHiem {
    private int maBH;
    private int maNV;
    private String hoTen;
    private String loaiBH;
    private String bdBH;
    private String ktBH;

    public loadBaoHiem() {
    }

    public loadBaoHiem(int maBH, int maNV, String hoTen, String loaiBH, String bdBH, String ktBH) {
        this.maBH = maBH;
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.loaiBH = loaiBH;
        this.bdBH = bdBH;
        this.ktBH = ktBH;
    }

    /**
     * @return the maNV
     */
    public int getMaNV() {
        return maNV;
    }

    /**
     * @param maNV the maNV to set
     */
    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    /**
     * @return the tenNV
     */
    public String getHoTen() {
        return hoTen;
    }

    /**
     * @param tenNV the tenNV to set
     */
    public void setHoTen(String tenNV) {
        this.hoTen = tenNV;
    }

    /**
     * @return the loaiBH
     */
    public String getLoaiBH() {
        return loaiBH;
    }

    /**
     * @param loaiBH the loaiBH to set
     */
    public void setLoaiBH(String loaiBH) {
        this.loaiBH = loaiBH;
    }

    /**
     * @return the bdBH
     */
    public String getBdBH() {
        return bdBH;
    }

    /**
     * @param bdBH the bdBH to set
     */
    public void setBdBH(String bdBH) {
        this.bdBH = bdBH;
    }

    /**
     * @return the ktBH
     */
    public String getKtBH() {
        return ktBH;
    }

    /**
     * @param ktBH the ktBH to set
     */
    public void setKtBH(String ktBH) {
        this.ktBH = ktBH;
    }

    /**
     * @return the maBH
     */
    public int getMaBH() {
        return maBH;
    }

    /**
     * @param maBH the maBH to set
     */
    public void setMaBH(int maBH) {
        this.maBH = maBH;
    }


}
