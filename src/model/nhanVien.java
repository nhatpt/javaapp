/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class nhanVien {
    private int maNV ;
    private String hoTen;
    private int CMND;
    private int GT;
    private String SDT;
    private String ngaySinh;
    private String  diaChi;
    private String queQuan;
    private int maCV;
    private int maTDHV;
    private int maPB;
    private int maLogin;
    private int bacLuong;

    public nhanVien() {
    }

    public nhanVien(int maNV, String hoTen, int CMND, int GT, String SDT, String ngaySinh, String diaChi, String queQuan, int maCV, int maTDHV, int maPB, int maLogin, int bacLuong) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.CMND = CMND;
        this.GT = GT;
        this.SDT = SDT;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
        this.queQuan = queQuan;
        this.maCV = maCV;
        this.maTDHV = maTDHV;
        this.maPB = maPB;
        this.maLogin = maLogin;
        this.bacLuong = bacLuong;
    }

    public int getMaCV() {
        return maCV;
    }

    public void setMaCV(int maCV) {
        this.maCV = maCV;
    }

    

    public int getBacLuong() {
        return bacLuong;
    }

    public int getCMND() {
        return CMND;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public int getGT() {
        return GT;
    }

    public String getHoTen() {
        return hoTen;
    }

    public int getMaLogin() {
        return maLogin;
    }

    public int getMaNV() {
        return maNV;
    }

    public int getMaPB() {
        return maPB;
    }

    public int getMaTDHV() {
        return maTDHV;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public String getSDT() {
        return SDT;
    }

    public void setBacLuong(int bacLuong) {
        this.bacLuong = bacLuong;
    }

    public void setCMND(int CMND) {
        this.CMND = CMND;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public void setGT(int GT) {
        this.GT = GT;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setMaLogin(int maLogin) {
        this.maLogin = maLogin;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    public void setMaPB(int maPB) {
        this.maPB = maPB;
    }

    public void setMaTDHV(int maTDHV) {
        this.maTDHV = maTDHV;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }
}
