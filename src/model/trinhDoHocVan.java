/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class trinhDoHocVan {
    private int maTDHv;
    private String tenTDHV;

    public trinhDoHocVan() {
    }

    public trinhDoHocVan(int maTDHv, String tenTDHV) {
        this.maTDHv = maTDHv;
        this.tenTDHV = tenTDHV;
    }

    public int getMaTDHv() {
        return maTDHv;
    }

    public String getTenTDHV() {
        return tenTDHV;
    }

    public void setMaTDHv(int maTDHv) {
        this.maTDHv = maTDHv;
    }

    public void setTenTDHV(String tenTDHV) {
        this.tenTDHV = tenTDHV;
    }
    
}
