/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NgocChau
 */
public class Loadsalary {
    private int maNV;
    private String hoTen;
    private String CMND;
    private String tenCV;
    private String tenPB;
    private double luongCoBan;
    private double heSoLuong;
    private double heSoPhuCap;
    private double bacLuong;

    public Loadsalary(){
    }
    public Loadsalary(int maNV, String hoTen, String CMND, String tenCV, String tenPB) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.CMND = CMND;
        this.tenCV = tenCV;
        this.tenPB = tenPB;
        
    }
    public Loadsalary(int maNV, String hoTen, String CMND, String tenCV, String tenPB, double luongCoBan, double heSoLuong, double heSoPhuCap) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.CMND = CMND;
        this.tenCV = tenCV;
        this.tenPB = tenPB;
        this.luongCoBan = luongCoBan;
        this.heSoLuong = heSoLuong;
        this.heSoPhuCap = heSoPhuCap;
    }

    public Loadsalary(int maNV, String hoTen, String CMND, String tenCV, String tenPB, double luongCoBan, double heSoLuong, double heSoPhuCap, double bacLuong) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.CMND = CMND;
        this.tenCV = tenCV;
        this.tenPB = tenPB;
        this.luongCoBan = luongCoBan;
        this.heSoLuong = heSoLuong;
        this.heSoPhuCap = heSoPhuCap;
        this.bacLuong = bacLuong;
    }

    public double getBacLuong() {
        return bacLuong;
    }

    public void setBacLuong(double bacLuong) {
        this.bacLuong = bacLuong;
    }

   
    

    public int getMaNV(){
        return maNV;
    }
    public String getHoTen(){
        return hoTen;
    }

    public String getCMND() {
        return CMND;
    }


    public double getHeSoLuong() {
        return heSoLuong;
    }

    public double getHeSoPhuCap() {
        return heSoPhuCap;
    }

    public double getLuongCoBan() {
        return luongCoBan;
    }

    public String getTenCV() {
        return tenCV;
    }

    public String getTenPB() {
        return tenPB;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

 

    public void setHeSoLuong(double heSoLuong) {
        this.heSoLuong = heSoLuong;
    }

    public void setHeSoPhuCap(double heSoPhuCap) {
        this.heSoPhuCap = heSoPhuCap;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setLuongCoBan(double luongCoBan) {
        this.luongCoBan = luongCoBan;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    public void setTenCV(String tenCV) {
        this.tenCV = tenCV;
    }

    public void setTenPB(String tenPB) {
        this.tenPB = tenPB;
    }
    
}

