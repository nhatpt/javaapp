/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class phongBan {
    private int maPB;
    private String tenPB;
    private String SDTPB;

    public phongBan() {
    }

    public phongBan(int maPB, String tenPB, String SDTPB) {
        this.maPB = maPB;
        this.tenPB = tenPB;
        this.SDTPB = SDTPB;
    }

    public int getMaPB() {
        return maPB;
    }

    public String getSDTPB() {
        return SDTPB;
    }

    public String getTenPB() {
        return tenPB;
    }

    public void setMaPB(int maPB) {
        this.maPB = maPB;
    }

    public void setSDTPB(String SDTPB) {
        this.SDTPB = SDTPB;
    }

    public void setTenPB(String tenPB) {
        this.tenPB = tenPB;
    }
    
}
