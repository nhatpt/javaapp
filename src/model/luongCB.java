/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NgocChau
 */
public class luongCB {
    private int bacLuong;
    private double luongCoBan;

    public luongCB() {
    }

    public luongCB(int bacLuong, double luongCoBan) {
        this.bacLuong = bacLuong;
        this.luongCoBan = luongCoBan;
    }

    public int getBacLuong() {
        return bacLuong;
    }

    public double getLuongCoBan() {
        return luongCoBan;
    }

    public void setBacLuong(int bacLuong) {
        this.bacLuong = bacLuong;
    }

    public void setLuongCoBan(double luongCoBan) {
        this.luongCoBan = luongCoBan;
    }
    
}
