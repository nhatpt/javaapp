/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Long
 */
public class loadDSQLNghiPhep {
    
    private int maQLNghiPhep;

    private int maNV;
    private String tenNV;
    private String phongBan;
    private String lyDo;
    private String fromDate;
    private String toDate;
    private  int trangThai;
    private String moTa;

    public loadDSQLNghiPhep() {
    }

    public loadDSQLNghiPhep(int maQLNghiPhep, int maNV, String tenNV, String phongBan, String lyDo, String fromDate, String toDate, int trangThai, String moTa) {
        this.maQLNghiPhep = maQLNghiPhep;  
        this.maNV = maNV;
        this.tenNV = tenNV;
        this.phongBan = phongBan;
        this.lyDo = lyDo;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.trangThai = trangThai;
        this.moTa = moTa;
        
    }

    public String getMoTa() {
        return moTa;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    
    public int getMaQLNghiPhep() {
        return maQLNghiPhep;
    }

    public void setMaQLNghiPhep(int maQLNghiPhep) {
        this.maQLNghiPhep = maQLNghiPhep;
    }

    
    public String getFromDate() {
        return fromDate;
    }

    public String getLyDo() {
        return lyDo;
    }

    public int getMaNV() {
        return maNV;
    }


    public String getPhongBan() {
        return phongBan;
    }

    public String getTenNV() {
        return tenNV;
    }

    public String getToDate() {
        return toDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setLyDo(String lyDo) {
        this.lyDo = lyDo;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

  

    public void setPhongBan(String phongBan) {
        this.phongBan = phongBan;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
    
}
