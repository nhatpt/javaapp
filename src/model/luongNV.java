/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class luongNV {
    private int maQLLuong;
    private int bacLuong;
    private double heSoLuong;
    private double heSoPhuCap;
    private double luongCoBan;

    public luongNV() {
    }

    public luongNV(int maQLLuong, int bacLuong, double heSoLuong, double heSoPhuCap, double luongCoBan) {
        this.maQLLuong = maQLLuong;
        this.bacLuong = bacLuong;
        this.heSoLuong = heSoLuong;
        this.heSoPhuCap = heSoPhuCap;
        this.luongCoBan = luongCoBan;
    }


    public int getMaQLLuong() {
        return maQLLuong;
    }

    public void setMaQLLuong(int maQLLuong) {
        this.maQLLuong = maQLLuong;
    }


    public int getBacLuong() {
        return bacLuong;
    }

    public double getHeSoLuong() {
        return heSoLuong;
    }

    public double getHeSoPhuCap() {
        return heSoPhuCap;
    }

    public double getLuongCoBan() {
        return luongCoBan;
    }

    public void setBacLuong(int bacLuong) {
        this.bacLuong = bacLuong;
    }

    public void setHeSoLuong(double heSoLuong) {
        this.heSoLuong = heSoLuong;
    }

    public void setHeSoPhuCap(double heSoPhuCap) {
        this.heSoPhuCap = heSoPhuCap;
    }

    public void setLuongCoBan(double luongCoBan) {
        this.luongCoBan = luongCoBan;
    }
 
}
