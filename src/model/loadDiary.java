/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.logging.Logger;

/**
 *
 * @author Hades
 */
public class loadDiary {

    public loadDiary() {
    }
    

    public int getMaLoger() {
        return maLoger;
    }

    public String getUserName() {
        return userName;
    }

    public String getDetails() {
        return details;
    }

    public String getTimer() {
        return timer;
    }

    public String getFullName() {
        return fullName;
    }

    public int getMaNV() {
        return maNV;
    }

    public String getPhone() {
        return phone;
    }

    public String getPosition() {
        return position;
    }

    public void setMaLoger(int maLoger) {
        this.maLoger = maLoger;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public loadDiary(int maLoger, int maNV, String fullName, String phone, String position, String userName, String details, String timer) {
        this.maLoger = maLoger;
        this.maNV = maNV;
        this.fullName = fullName;
        this.phone = phone;
        this.position = position;
        this.userName = userName;
        this.details = details;
        this.timer = timer;
    }
    private int maLoger;
    private int maNV;
    private String fullName;
    private String phone;
    private String position;
    private String userName;
    private String details;
    private String timer;
    
}
