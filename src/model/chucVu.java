/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class chucVu {
    private int maCV;
    private String tenCV;

    public chucVu() {
    }

    public chucVu(int maCV, String tenCV) {
        this.maCV = maCV;
        this.tenCV = tenCV;
    }

    public int getMaCV() {
        return maCV;
    }

    public String getTenCV() {
        return tenCV;
    }

    public void setMaCV(int maCV) {
        this.maCV = maCV;
    }

    public void setTenCV(String tenCV) {
        this.tenCV = tenCV;
    }

}
